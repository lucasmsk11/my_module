from setuptools import setup, find_packages

setup(
    name='my_module',
    version='0.1',
    packages=find_packages(),
    install_requires=[],  # Lista de dependências, se houver
    author='LUKINHA',
    author_email='lucasmsk11@gmail.com',
    description='Uma breve descrição do seu módulo',
    url='https://gitlab.com/lucasmsk11/my_module/',
    license='MIT',  # Ou outra licença à sua escolha
)
